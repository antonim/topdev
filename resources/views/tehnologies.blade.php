
@extends('layouts.app')

@section('content')
        <div class="container">
            <!-- Content here -->


            <div class="row">
                <div class="col">
                    <div class="formatedtext">

                        <p>Ниже представлен список технологий, который постояно расширяется. Мы двигаемся в ногу со временем.</p>

                        <?php

                        $tehnologies = ['HTML5 + CSS', 'BOOTSTRAP 4', 'PHP 7.3',  'Zend 3', 'Laravel 5', 'JavaScript', 'ExtJs', 'jQuery', 'React', 'MySQL', 'PgSQL', 'Redis Cluster',  'Kibana + EKL', 'RabbitMQ', 'Agile', 'Scrum', 'High Load', 'DDD', 'TDD', 'CQRS']

                        ?>

                        <ul class="list-group">

                            @foreach($tehnologies as $tehnology)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{$tehnology}}
                                    {{--<span class="badge badge-primary badge-pill">14</span>--}}
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
@endsection
