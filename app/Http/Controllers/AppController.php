<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{


    public function index()
    {
        $isRoot = true;

        return view('welcome', ['isRoot' => $isRoot]);
    }

    public function webDevelopment()
    {
        return view('web_development');
    }

    public function tehnologies()
    {
        return view('tehnologies');
    }

    public function contactUs()
    {
        return view('contact_us');
    }
//
//Route::get('/', 'AppController@index');
//Route::get('/web_development', 'AppController@webDevelopment');
//Route::get('/tehnologies', 'AppController@tehnologies');
//Route::get('/contact_us', 'AppController@contactUs');

}
